.. langtonSim documentation master file, created by
   sphinx-quickstart on Fri Oct  9 12:27:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   


Welcome to langtonSim's documentation!
======================================

LangtonSim is a Langton's ant simulation application implemented in C++ and
Qt5.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   var_settings
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
