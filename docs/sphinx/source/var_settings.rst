Default application settings
============================

Default settings are set in `paramwidget.h` and `mainwindow.h` files. They
are related to the configuration of the SimulInstance class:

.. doxygenvariable:: NB_COL
.. doxygenvariable:: NB_ROW
.. doxygenvariable:: PIXEL
.. doxygenvariable:: NB_ANT
.. doxygenvariable:: SLEEP_TIME
.. doxygenvariable:: RANDOM_ORIENT
.. doxygenvariable:: ANT_ORIENT_POLICY


Or to the GUI itself:

.. doxygenvariable:: PATH_2_SIMUL_FILES
.. doxygenvariable:: MIN_PIX_SHOW_GRID

.. attention:: You should modified them if you wish to have a different behavior at the opening of the application.
