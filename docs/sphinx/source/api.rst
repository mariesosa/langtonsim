LangtonSim API
==============

Enumerations
------------

Ant's color and orientation are defined by the two following enumerations.

.. doxygenenum:: orientation
.. doxygenenum:: color_ant


Computational Classes
---------------------

.. doxygenclass:: Square
    :members:
  
.. doxygenclass:: Grid
    :members:

.. doxygenclass:: Ant
    :members:
    
.. doxygenclass:: CSVReadException
    :members:

.. doxygenclass:: SimulInstance
    :members:


Graphical classes
-----------------

.. doxygenclass:: ParamWidget
    :members:

.. doxygenclass:: MainWindow
    :members:
    
    
Useful functions
----------------

.. doxygenfunction:: colorAsQString
.. doxygenfunction:: orientAsQString
.. doxygenfunction:: associateColor

