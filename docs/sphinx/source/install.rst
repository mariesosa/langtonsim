Installation
============

langtonSim depends on `qtcsv <https://github.com/iamantony/qtcsv>`_ and
`FIcon <https://github.com/dridk/QFontIcon.git>`_. Both are included in this
project as sub-modules. To install the application, use the following
command::

    git clone --recurse-submodules https://gitlab.com/mariesosa/langtonsim.git


Then, go to the cloned directory and build the project::

    # Create a build directory
    mkdir build
    cd build

    # Build the project
    qmake ../langtonSim.pro
    make

You can now start the application with::
    
    ./langtonSim
