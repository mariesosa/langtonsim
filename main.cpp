#include "sources/mainwindow.h"
#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>

int main(int argc, char *argv[]){
    // Initialize the window
    QApplication a(argc, argv);
    a.setApplicationName("LangtonSim");

    MainWindow w;
    w.show();

    // Show the main window in the middle of the screen
    w.setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            w.size(),
            qApp->desktop()->availableGeometry()
        )
    );

    return a.exec();
}
