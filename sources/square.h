#ifndef SQUARE_H
#define SQUARE_H

#include <tuple>
#include <QString>
#include <QStringList>

/// The possible colors of an ant.
enum color_ant {RED, BLUE, GREEN, ORANGE, YELLOW, PURPLE, BLACK, WHITE};

/// The orientation of the ant on the square (U = up, D = down, L = left, R = right)
enum orientation {U, D, L, R};

constexpr std::initializer_list<orientation> all_orientation = {U, D, L, R};

// QStringList used to convert enum types to Qstring (and the other way around)
// in orientAsQString and colorAsQString functions.
static QStringList orientation_str = (QStringList() << "Up" << "Down" << "Left" << "Right");
static QStringList color_ant_str = (QStringList() << "RED" << "BLUE" << "GREEN" << "ORANGE"
                                    << "YELLOW" << "PURPLE" << "BLACK" << "WHITE");
/**
 * @brief A Square object is defined by its position and a background color.
 */
class Square {
private:
    int m_col;         ////< Column index
    int m_line;        ////< Line index
    color_ant m_color; ////< Background color

public:
    // Constructors
    Square();
    Square(int const & col, int const & line, color_ant const & color=WHITE);

    // Getter
    int const & getLine() const{return m_line;}
    int const & getCol() const{return m_col;}
    color_ant const & getColor() const{return m_color;}

    // Setter
    void setColor(color_ant const & color);
    void inverseColor(color_ant const & color);
};

// Functions to transform orientation and color_ant enumerations as QString
QString orientAsQString(orientation const & orient);
QString colorAsQString(color_ant const & color);

// Function to transform a QString as an orientation
orientation QStringAsOrient(QString const & orient);

#endif // SQUARE_H
