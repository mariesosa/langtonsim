#ifndef ANT_H
#define ANT_H
#include "grid.h"
#include <tuple>
#include <QDebug>
#include <QString>

/**
 * @brief A ant object is defined by its color, position and orientation on a grid.
 */
class Ant {
private:
    unsigned int m_id;    ///< Unique ID
    int m_col;            ///< Column index on the grid
    int m_line;           ///< Line index on the grid
    orientation m_orient; ///< Orientation on the grid (U, D, L or R)
    color_ant m_color;    ///< Associated color
    bool m_is_dead;       ///< Specify if the ant is dead (i.e. outside the grid)

public:
    static unsigned int _count; ////< Counter used to assign a unique ID to each ant

    // Constructors
    Ant(Grid & grid, color_ant const & color);
    Ant(Grid & grid, orientation const & orient, color_ant const & color);
    Ant(int const & col, int const & line, orientation const & orient = U, color_ant const & color = RED);

    // Copy constructor
    Ant(const Ant & ant);

    // Getter
    color_ant const & getColor() const{return m_color;}
    orientation const & getOrient() const{return m_orient;}
    int const & getLine() const{return m_line;}
    int const & getCol() const{return m_col;}
    unsigned int const & getId() const{return m_id;}
    std::tuple<int,int> const getPosition(){return std::make_tuple(m_col, m_line);}
    bool const & isDead() const {return m_is_dead;}

    // Get color and orientation values as QStrings instead of enum types
    QString getColorAsQString() const;
    QString getOrientAsQString() const;

    // Move the ant on the grid
    bool move(Grid const & grid);
};

// Functions used to print Ant objects
std::ostream & operator<< (std::ostream & os, const Ant & ant);
QDebug operator<< (QDebug debug, const Ant & ant);

#endif // ANT_H
