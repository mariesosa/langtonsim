#ifndef SIMULINSTANCE_H
#define SIMULINSTANCE_H

// Custom
#include "ant.h"
#include "grid.h"

// Standard
#include <vector>
#include <tuple>

// Graphical
#include <QObject>

/**
 * @brief Custom Exception class to handle problems related to CSV reading issues
 */
class CSVReadException : public std::exception {
private:
    const char* m_msg;  ///< Detailed exception message to show
    const char* m_file; ///< File for which the exception occurs
    int m_line;         ///< Line number in which the exception occurs

public:
    // Constructor
    CSVReadException(const char* msg, const char* file, int line);

    // Getter
    const char* get_msg() const { return m_msg; }
    const char* get_file() const { return m_file; }
    int get_line() const { return m_line; }

    const char * what () const throw (){return "CSVReadException: ";}
};

/**
 * @brief Represent a langton's ant simulation.
 *
 * A simulation object consists of a grid and a vector of ants moving on it.
 * The simulation terminates when all ants are dead (i.e. outside the grid).
 */
class SimulInstance : public QObject {
    Q_OBJECT

private:
    int m_nb_iter;  ///< Number of iterations of the simulation
    int m_sleep;    ///< Sleep time between each iteration of the simulation
    bool m_is_dead; ///< Specify if all ants are dead (i.e. out of the grid)
    bool m_go_on;   ///< Specify if the simulation should continue or not (used in run function)
    int m_nb_col;   ///< Number of column in the grid
    int m_nb_row;   ///< Number of rows in the grid
    Grid *m_grid;   ///< Grid of size m_nb_col * m_nb_row
    int m_nb_ant;   ///< Number of ants on the grid
    QString m_ant_orient_policy;  ///< Orientation policy of the ants of `m_ants`
    std::vector<Ant> m_ants;      ///< Ants object present on the grid
    std::vector<Ant> m_ants_init; ///< The initial vector of ants on the grid

    // Private methods to initialize the grid and the vector of ants
    void initGrid(QString const & orient);
    void initAntsRandom();
    void initAntsOrient(const orientation & orient);

public:
    // Constructor
    SimulInstance(int const & nb_col, int const & nb_row, int const & nb_ant,
                  int const & sleep, QString const & orient);

    // Getter
    Grid * getGrid() const{return m_grid;}
    std::vector<Ant> getAnts() const{return m_ants;}
    int const & getWidthGrid() const {return m_grid->getWidth();}
    int const & getHeightGrid() const {return m_grid->getHeight();}

    // Destructor
    ~SimulInstance();

public slots:
    // Handle simulation actions
    void oneStep();
    void run();
    void pause();
    void stop();

    // Create a new simulation (with new ant positions, etc.)
    void createNewSimulation(int const & nb_col, int const & nb_row, int const & nb_ant,
                             int const & sleep, QString const & orient);

    // Reset the simulation to its initial state
    void resetToInit();

    // Update the sleep attribute of the simulation object
    void updateSleep(int const & sleep);

    // Save and load a simulation to/from a CSV file
    void saveSimulationAsCSV(QString const & filepath, int const & pixel);
    void loadSimulationFromCSV(QString const & filepath);

signals:
    /**
     * @brief Emited at each step of the simulation, to notify the new positions of the ants on the grid.
     */
    void simulValueUpdated(Grid *grid, std::vector<Ant> const & ants,
                           std::vector<std::tuple<int,int>> position_2_update, int const & nb_iter);

    /**
     * @brief Emited when the grid and ants variables have been reseted.
     */
    void simulValueReseted(Grid *grid, std::vector<Ant> ants);

    /**
     * @brief Emited when the simulation is terminated, i.e. when all ants are dead.
     */
    void simulFinished();

    /**
     * @brief Emited when a simulation file has been successfully loaded.
     */
    void simulLoaded(Grid *grid, std::vector<Ant> const & ants, int const & pixel_size,
                     QString const & orient);
    /**
     * @brief Emited when an error occurred during the loading of a simulation file.
     */
    void errorFound(QString const & short_msg, CSVReadException const & error);
};

#endif // SIMULINSTANCE_H
