#include <iostream>
#include <string>
#include "grid.h"

/**
 * @brief Construct a Grid object with given width and height.
 * @param width Width of the grid
 * @param height Height of the grid
 */
Grid::Grid(int const & width, int const & height){
    m_height = height;
    m_width = width;
    m_tab = new Square*[m_width]; // number of columns
    for(int col = 0; col < m_width; col++)
        m_tab[col] = new Square[m_height]; // number of lines by columns

    for(int col = 0; col < m_width; col++) {
        for(int line = 0; line < m_height; line++)
            m_tab[col][line] = Square(col, line);
    }
}

/**
 * @brief Copy constructor.
 */
Grid::Grid(const Grid &obj){
    m_height = obj.m_height;
    m_width = obj.m_width;
    m_tab = new Square*[m_width];
    for(int col = 0; col < m_width; col++)
        m_tab[col] = new Square[m_height];

    for(int col = 0; col < m_width; col++) {
        for(int line = 0; line < m_height; line++)
            m_tab[col][line] = obj.m_tab[col][line];
    }
}

/**
 * @brief Affectation operator.
 */
Grid&Grid::operator=(const Grid &obj){
    m_height = obj.m_height;
    m_width = obj.m_width;
    m_tab = new Square*[m_width];
    for(int col = 0; col < m_width; col++)
        m_tab[col] = new Square[m_height];

    for(int col = 0; col < m_width; col++) {
        for(int line = 0; line < m_height; line++)
            m_tab[col][line] = obj.m_tab[col][line];
    }

    return *this;
}

/**
 * @brief Destructor.
 */
Grid::~Grid(){
    for(int col = 0; col < m_width; col++)
        delete [] m_tab[col];
    delete [] m_tab;
}

/**
 * @brief Overload of operator ++.
 */
Grid& Grid::operator++(){
    m_width += 2;
    m_height += 2;

    Square **m_tab2;
    m_tab2 = new Square*[m_width];
    for(int col = 0; col < m_width; col++)
        m_tab2[col] = new Square[m_height];

    for(int col = 0; col < m_width; col++) {
        for(int line = 0; line < m_height; line++)
            m_tab2[col][line] = Square(col, line);
    }

    for(int col = 0; col < m_width-2; col++) {
        for(int line = 0; line < m_height-2; line++) {
            m_tab2[col+1][line+1].setColor(m_tab[col][line].getColor());
        }
    }

    m_tab = m_tab2;
    return *this;
}

/**
 * @brief Inverse the background color of the square at position (`col`, `line`) on the grid.
 * @param col Column index of the square on the grid
 * @param line Line index of the square on the grid
 * @param color Background color to consider for the inversion of color
 * @see Square::inverseColor
 */
void Grid::inverseColor(const int & col, const int & line, const color_ant & color){
    m_tab[col][line].inverseColor(color);
}

/**
 * @brief Set the background color of the square at position (`col`, `line`) on the grid.
 * @param col Column index of the square on the grid
 * @param line Line index of the square on the grid
 * @param color Background color to assign to the square
 * @see Square::setColor
 */
void Grid::setColor(const int & col, const int & line, const color_ant & color){
     m_tab[col][line].setColor(color);
}
