// Custom
#include "ant.h"
#include "grid.h"

// Standard
#include <iostream>
#include <stdlib.h>

using namespace std;

// Graphical
#include <QDebug>
#include <QString>

// Initialize the ant counter
unsigned int Ant::_count(0);

/**
 * @brief Create an ant with the given orientation and color at a random position on the grid.
 * @param grid Grid in which the ant moves
 * @param orient Ant's orientation on the grid
 * @param color Ant's color
 */
Ant::Ant(Grid & grid, orientation const & orient, color_ant const & color):
    m_id(_count++), m_orient(orient), m_color(color), m_is_dead(false){
    int max_line = grid.getHeight();
    int max_col = grid.getWidth();

    // Assign a random position to the ant
    m_line = rand() % (max_line - 4) + 2;
    m_col = rand() % (max_col - 4) + 2;
}

/**
 * @brief Create an ant with the given color at a random position and orientation on the grid
 * @param grid Grid in which the ant moves
 * @param color Ant's color
 */
Ant::Ant(Grid & grid, color_ant const & color): m_id(_count++), m_color(color), m_is_dead(false){
    int max_line = grid.getHeight();
    int max_col = grid.getWidth();

    // Assign a random position to the ant
    m_line = rand() % (max_line - 4) + 2;
    m_col = rand() % (max_col - 4) + 2;

    // Assign a random orientation to the ant
    m_orient = (orientation) (rand() % 4);
}

/**
 * @brief Create an ant at position (`col`, `line`) with orientation `orient` and color `color`.
 * @param col Column index on the grid
 * @param line Line index on the grid
 * @param orient Ant's orientation on the grid
 * @param color Ant's color
 */
Ant::Ant(int const & col, int const & line, orientation const & orient, color_ant const & color)
    :m_id(_count++), m_col(col), m_line(line), m_orient(orient), m_color(color), m_is_dead(false){
}

/**
 * @brief Copy constructor
 */
Ant::Ant(const Ant &ant){
    m_col = ant.getCol();
    m_line = ant.getLine();
    m_color = ant.getColor();
    m_orient = ant.getOrient();
    m_is_dead = ant.isDead();
    m_id = ant.getId();
}

/**
 * Move the Ant on the grid according to the following rules:
 * 1. When a ant leave a square, its color is inversed.
 * 2. If the ant is on a white square, it moves to the left.
 * 3. Otherwise, it moves to the right.
 *
 * @param grid The grid on which move the ant
 * @return a boolean specifying if the ant is dead (i.e. outside the grid).
 */
bool Ant::move(Grid const & grid){
    // Get the square color
    color_ant oldm_color = grid.getColor(m_col, m_line);

    // If the ant is on a white square, it moves to the left
    if (oldm_color == WHITE) {
        switch (m_orient) {
            case U:
                m_orient = L;
                m_col--;
                break;
            case L:
                m_orient = D;
                m_line++;
                break;
            case D:
                m_orient = R;
                m_col++;
                break;
            case R:
                m_orient = U;
                m_line--;
                break;
        }
    }
    // Otherwise, the ant moves to the right
    else {
        switch (m_orient) {
            case U:
                m_orient = R;
                m_col++;
                break;
            case L:
                m_orient = U;
                m_line--;
                break;
            case D:
                m_orient = L;
                m_col--;
                break;
            case R:
                m_orient = D;
                m_line++;
                break;
        }
    }

    // Check if the ant is dead (i.e. outside the grid)
    int width = grid.getWidth();
    int height = grid.getHeight();
    m_is_dead = 0 > m_col || m_col >= width || 0 > m_line || m_line >= height;
    return m_is_dead;
}

// Functions used to print Ant objects
std::ostream & operator<< (std::ostream & os, const Ant & ant){
    os << "Ant " << ant.getId() << " : color = " << ant.getColor();
    os << " - pos = (" << ant.getCol() << "," << ant.getLine() << ")";
    return os;
}

QDebug operator<<(QDebug debug, const Ant & ant){
    debug.setAutoInsertSpaces(false);
    debug << "Ant " << ant.getId() << " : color = " << ant.getColorAsQString();
    debug << " - pos = (" << ant.getCol() << "," << ant.getLine() << ")";
    debug << " - orient = " << ant.getOrientAsQString();
    return debug.maybeSpace();
}

// Functions used for qDebug prints
/**
 * @brief Return the ant's orientation as a QString.
 * @see orientAsQString
 */
QString Ant::getOrientAsQString() const{
    return orientAsQString(m_orient);
}

/**
 * @brief Return the ant's color as a QString.
 * @see colorAsQString
 */
QString Ant::getColorAsQString() const{
    return colorAsQString(m_color);
}
