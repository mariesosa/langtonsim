#ifndef GRID_H
#define GRID_H
#include "square.h"

/**
 * @brief A Grid is defined by its width, height and array of Square.
 */
class Grid {
private:
    int m_height;   ////< Height of the grid
    int m_width;    ////< Width of the grid
    Square **m_tab; ////< Array of Square pointers

public:
    Grid(int const & width = 60, int const & height = 60); // Grid constructor
    Grid(const Grid & obj);                                // Copy Constructor
    Grid& operator=(const Grid & obj);                     // Affectation operator
    Grid& operator++(void);                                // Overload of operator ++

    // Getter
    int const & getHeight() const{return m_height;}
    int const & getWidth() const{return m_width;}
    color_ant const & getColor(const int & col, const int & line) const {return m_tab[col][line].getColor();}
    Square const & getSquare(const int & col, const int & line) const{return m_tab[col][line];}

    // Setter
    void inverseColor(const int & col, const int & line, const color_ant & color);
    void setColor(const int & col, const int & line, const color_ant & color);

    // Destructor
    ~Grid();
};


#endif // GRID_H
