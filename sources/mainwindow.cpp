// Standard
#include <iostream>
#include <vector>
#include <stdlib.h>

using namespace std;

// Custom
#include "mainwindow.h"
#include "paramwidget.h"
#include "simulinstance.h"

// Graphical
#include <QApplication>
#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QColor>
#include <QString>
#include <QPushButton>
#include <QIcon>
#include <QSpinBox>
#include <QFormLayout>
#include <QButtonGroup>
#include <QThread>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QKeySequence>
#include <QApplication>
#include <QDesktopWidget>
#include "lib/QFontIcon/QFontIcon/qfonticon.h"


/**
 * @brief Construct a MainWindow which instantiate a simulation with a grid of size `nb_col` x `nb_row`
 * and represent it with a pixel size of `pixel`.
 * @param parent Its parent
 * @param nb_col Number of column of the grid
 * @param nb_row Number of row of the grid
 * @param pixel Pixel size of the grid
 * @param sleep Sleeping time between each iteration of the simulation.
 */
MainWindow::MainWindow(QWidget *parent, int const & nb_col, int const & nb_row, int const & pixel,
                       int const & sleep)
    : QMainWindow(parent), m_nb_col(nb_col), m_nb_row(nb_row), m_pixel(pixel), m_sleep(sleep),
      m_nb_ant(NB_ANT), m_orient(true){

    // Configure QFontIcons to use Font Awesome style
    QFontIcon::addFont(":/FontAwesome.ttf");

    QWidget *widget = new QWidget;
    QHBoxLayout *main_layout = new QHBoxLayout(widget);
    QVBoxLayout *grid_layout = new QVBoxLayout();
    QVBoxLayout *control = new QVBoxLayout();
    QHBoxLayout *button_layout = new QHBoxLayout();
    m_label = new QLabel();
    m_iterations = new QLabel();

    // Grid layout
    m_pix = new QPixmap(m_nb_col*m_pixel + 1, m_nb_row*m_pixel + 1);
    m_pix->fill(Qt::white);
    m_painter = new QPainter(m_pix);
    m_label->setPixmap(*m_pix);
    grid_layout->addWidget(m_label);
    grid_layout->setAlignment(m_label, Qt::AlignCenter);

    // Iteration widget
    QString text(QString("Iter : ") + QString::number(0));
    m_iterations->setText(text);
    grid_layout->addWidget(m_iterations);
    main_layout->addLayout(grid_layout, 1);

    // Button layout
    m_but_step = new QPushButton(FIcon(0xf051), ""); // 1-step (forward)
    m_but_reset = new QPushButton(FIcon(0xf12d), ""); // undo
    m_but_new = new QPushButton(FIcon(0xf0e2 ), ""); // new
    m_but_save_simul = new QPushButton(FIcon(0xf0c7), ""); // save
    m_but_load_simul = new QPushButton(FIcon(0xf15b), ""); // upload

    // Play/pause button
    QIcon *ico = new QIcon();
    ico->addPixmap(QPixmap(":/pause"), QIcon::Normal, QIcon::On);
    ico->addPixmap(QPixmap(":/play"), QIcon::Normal, QIcon::Off);
    m_but_pause = new QPushButton();
    m_but_pause->setIcon(*ico);
    m_but_pause->setCheckable(true);

    // Add shortcuts for save and load buttons
    m_but_save_simul->setShortcut(QKeySequence::Save);
    m_but_load_simul->setShortcut(QKeySequence::Open);

    // Add tooltips to buttons
    m_but_pause->setToolTip("Play/stop the simulation");
    m_but_step->setToolTip("Perform 1 step of the simulation");
    m_but_reset->setToolTip("Reset the simulation");
    m_but_new->setToolTip("Initialize a new simulation");
    m_but_save_simul->setToolTip("Save the simulation");
    m_but_load_simul->setToolTip("Open a simulation");

    // Add the buttons to the layout
    button_layout->addWidget(m_but_pause);
    button_layout->addWidget(m_but_step);
    button_layout->addWidget(m_but_reset);
    button_layout->addWidget(m_but_new);
    button_layout->addWidget(m_but_load_simul);
    button_layout->addWidget(m_but_save_simul);
    control->addLayout(button_layout);

    // Enable/disable buttons
    connect(m_but_pause, &QPushButton::clicked, this, &MainWindow::onPausePressed);
    connect(m_but_reset, &QPushButton::clicked, this, &MainWindow::onResetPressed);
    connect(m_but_new, &QPushButton::clicked, this, &MainWindow::onResetPressed);

    // Initialize rand (used for ant position)
    srand(time(NULL));

    // Create a simulation object
    m_simulation = new SimulInstance(m_nb_col, m_nb_row, m_nb_ant, m_sleep, ANT_ORIENT_POLICY);

    // Initialize the graphical grid
    this->initGui(m_simulation->getGrid(), m_simulation->getAnts());

    // Add the simulation object to a thread
    m_simulThread = new QThread;
    m_simulThread->start();
    connect(m_simulThread, &QThread::finished, m_simulation, &QObject::deleteLater);

    // Connect the simulation signals/slots to the main window
    connect(m_simulation, &SimulInstance::simulValueUpdated, this, &MainWindow::updateGui);
    connect(m_simulation, &SimulInstance::simulValueReseted, this, &MainWindow::resetGui);
    connect(m_simulation, &SimulInstance::simulFinished, this, &MainWindow::endOfSimulation);
    connect(m_simulation, &SimulInstance::simulLoaded, this, &MainWindow::resetGuiAfterLoad);
    connect(this, &MainWindow::newSleepValue, m_simulation, &SimulInstance::updateSleep);
    connect(this, &MainWindow::simulationToSave, m_simulation, &SimulInstance::saveSimulationAsCSV);
    connect(this, &MainWindow::simulationToLoad, m_simulation, &SimulInstance::loadSimulationFromCSV);
    connect(this, &MainWindow::stopSimulation, m_simulation, &SimulInstance::stop);
    connect(m_simulation, &SimulInstance::errorFound, this, &MainWindow::showErrorMessage);

    // Connect buttons to actions
    connect(m_but_step, &QPushButton::clicked, m_simulation, &SimulInstance::oneStep);
    connect(m_but_pause, &QPushButton::clicked, m_simulation, &SimulInstance::pause);
    connect(m_but_reset, &QPushButton::clicked, m_simulation, &SimulInstance::resetToInit);
    connect(m_but_new, &QPushButton::clicked, this, &MainWindow::resetSimulation);
    connect(this, &MainWindow::resetSimulValues, m_simulation, &SimulInstance::createNewSimulation);
    connect(m_but_save_simul, &QPushButton::clicked, this, &MainWindow::saveToCSV);
    connect(m_but_load_simul, &QPushButton::clicked, this, &MainWindow::loadCSV);

    // Parameter layout: allow to modify the height, width and pixel's size of the grid, etc.
    ParamWidget *param_widget = new ParamWidget();
    connect(param_widget, &ParamWidget::valueChanged, this, &MainWindow::updateParam);
    connect(this, &MainWindow::newParamFromLoad, param_widget, &ParamWidget::setValues);

    control->addWidget(param_widget);
    main_layout->addLayout(control);

    // Perform main windows settings
    this->setCentralWidget(widget);
    this->setWindowIcon(QIcon(":/app"));

    // Verify that PATH_2_SIMUL_FILES exists
    QDir dirpath(PATH_2_SIMUL_FILES);
    if (dirpath.exists() == false){
        dirpath.mkpath(PATH_2_SIMUL_FILES);
    }
}

/**
 * @brief Return the QColor associated to the given color_ant value `color`.
 * @param color Color value to map
 * @return The QColor associated to `color`
 */
QColor associateColor(color_ant const & color){
    switch (color) {
        case RED:
            return Qt::red;
        case BLUE:
            return Qt::blue;
        case GREEN:
            return Qt::darkGreen;
        case ORANGE:
            return QColor(255, 175, 0);
        case YELLOW:
            return Qt::yellow;
        case PURPLE:
            return Qt::darkMagenta;
        case WHITE:
            return Qt::white;
        default:
            return Qt::black;
    }
}

/**
 * @brief Draw the Ant `ant` on the grid.
 * @param ant The ant to draw
 */
void MainWindow::drawAnt(Ant const & ant){
    int col = ant.getCol();
    int line = ant.getLine();

    if (m_pixel < MIN_PIX_SHOW_GRID) {
        // If the pixel size is small, we do not draw the triangle
        m_painter->setBrush(Qt::black);
        m_painter->setPen(Qt::black);
        m_painter->drawRect(col*m_pixel, line*m_pixel, m_pixel, m_pixel);
    } else {
        m_painter->setBrush(associateColor(ant.getColor()));
        orientation orient = ant.getOrient();
        QPolygon poly;
        int mid = m_pixel/2;

        // Define the ant current direction as a triangle
        // (up : |^|, down : |v|, left : |>|, or right : |<|)
        switch (orient) {
            case U:
                poly << QPoint(col*m_pixel, line*m_pixel + m_pixel);
                poly << QPoint(col*m_pixel + mid, line*m_pixel);
                poly << QPoint(col*m_pixel + m_pixel, line*m_pixel + m_pixel);
                break;
            case L:
                poly << QPoint(col*m_pixel + m_pixel, line*m_pixel);
                poly << QPoint(col*m_pixel, line*m_pixel + mid);
                poly << QPoint(col*m_pixel + m_pixel, line*m_pixel + m_pixel);
                break;
            case D:
                poly << QPoint(col*m_pixel, line*m_pixel);
                poly << QPoint(col*m_pixel + mid, line*m_pixel + m_pixel);
                poly << QPoint(col*m_pixel + m_pixel, line*m_pixel);
                break;
            default:
                poly << QPoint(col*m_pixel, line*m_pixel);
                poly << QPoint(col*m_pixel + m_pixel, line*m_pixel + mid);
                poly << QPoint(col*m_pixel, line*m_pixel + m_pixel);
                break;
        }
        m_painter->drawPolygon(poly);
    }
}

/**
 * @brief Update the background color of the Square at position (`col`, `line`) on the Pixmap.
 * @param grid Grid to consider
 * @param col Column index of the square on the grid
 * @param line Line index of the square on the grid
 */
void MainWindow::updateSquare(Grid *grid, int const & col, int const & line){
    /* Update the background color of a given square on the Pixmap */
    QColor color = associateColor(grid->getColor(col, line));
    m_painter->setBrush(color);
    if (m_pixel < MIN_PIX_SHOW_GRID)
        m_painter->setPen(color);
    else
        m_painter->setPen(Qt::black);
    m_painter->drawRect(col*m_pixel, line*m_pixel, m_pixel, m_pixel);
}

/**
 * @brief Re-Allocate the QPixmap object.
 */
void MainWindow::ReAllocPixmap(){
    m_pix = new QPixmap(m_nb_col*m_pixel + 1, m_nb_row*m_pixel + 1);
    m_painter = new QPainter(m_pix);
}

/**
 * @brief Initialize the QPixmap object according to `grid` and `ants` values.
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 */
void MainWindow::initPixmap(Grid *grid, vector<Ant> const & ants){
    int height = grid->getHeight();
    int width = grid->getWidth();
    for(int line = 0; line < height; line++)
        for(int col = 0; col < width; col++)
            this->updateSquare(grid, col, line);

    for(auto ant : ants)
        this->drawAnt(ant);
}

/**
 * @brief Update the Pixmap object (repaint only the given squares of the grid and repaint ant).
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 * @param position_2_update Vector of old positions of ants
 */
void MainWindow::updatePixmap(Grid *grid, vector<Ant> const & ants,
                              vector<tuple<int,int>> const & position_2_update){
    for (auto & pos : position_2_update)
        this->updateSquare(grid, get<0>(pos), get<1>(pos));

    for (auto & ant : ants)
        this->drawAnt(ant);
    m_label->setPixmap(*m_pix);
}

// Slots

/**
 * @brief Initialize/show the graphical grid.
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 */
void MainWindow::initGui(Grid *grid, vector<Ant> const & ants){
    this->initPixmap(grid, ants);
    m_label->setPixmap(*m_pix);
    qApp->processEvents();
}

/**
 * @brief Update the GUI after changes.
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 * @param position_2_update Vector of old positions of ants
 * @param nb_iter Number of iterations of the simulation
 * @see updatePixmap, updateIteration
 */
void MainWindow::updateGui(Grid *grid, vector<Ant> const & ants,
                           vector<tuple<int,int>> position_2_update, int const & nb_iter){
    // Update the pixmap
    this->updatePixmap(grid, ants, position_2_update);
    this->updateIteration(nb_iter);
    qApp->processEvents();
}

/**
 * @brief Reset the GUI for a new simulation (re-initialize the grid representation).
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 */
void MainWindow::resetGui(Grid *grid, vector<Ant> const & ants){
    // Reset the grid and pixmap
    m_pix->fill(Qt::white);
    m_label->setPixmap(*m_pix);

    // Show the grid in the window
    this->initGui(grid, ants);
    this->updateIteration(0);

    // Update the window size and center it on the screen
    this->adjustSize();
    this->setGeometry(
        QStyle::alignedRect(
            Qt::LeftToRight,
            Qt::AlignCenter,
            this->size(),
            qApp->desktop()->availableGeometry()
        )
    );

    qApp->processEvents();
}

/**
 * @brief Reset the GUI after the loading of a simulation file.
 * @param grid Grid to represent
 * @param ants Vector of ants moving on the grid
 * @param pixel_size Pixel size of the grid
 * @param orient Orientation policy of the ants of `ants`
 */
void MainWindow::resetGuiAfterLoad(Grid *grid, vector<Ant> const & ants, int const & pixel_size,
                                   QString const & orient){
    // Update attributes values
    m_nb_col = grid->getWidth();
    m_nb_row = grid->getHeight();
    m_nb_ant = ants.size();
    m_pixel = pixel_size;
    m_orient = orient;
    m_sleep = 0;

    // Reset the grid and pixmap
    this->ReAllocPixmap();
    this->resetGui(grid, ants);

    // Update param_widget to fit simulation values
    emit newParamFromLoad(m_nb_col, m_nb_row, m_pixel, m_sleep, m_nb_ant, m_orient);
}

/**
 * @brief Ask the simulation to re-create a new simulation.
 * @see resetSimulValues
 */
void MainWindow::resetSimulation(){
    emit resetSimulValues(m_nb_col, m_nb_row, m_nb_ant, m_sleep, m_orient);
}

/**
 * @brief Show an error occuring during the opening of a simulation file into a QMessageBox.
 * @param short_msg Summary of the error
 * @param error Exception object throwed by the SimulInstance object
 * @see CSVReadException
 */
void MainWindow::showErrorMessage(QString const & short_msg, CSVReadException const & error){
    QString message = QString("<p><b>%1</b></p><p>%2%3.<br/>In file: %4 - line: %5</p>")
            .arg(short_msg).arg(error.what()).arg(error.get_msg()).
            arg(error.get_file()).arg(error.get_line());
    QMessageBox::critical(this, "", message);
}

/**
 * @brief Define the GUI behaviour at the end of a simulation.
 */
void MainWindow::endOfSimulation(){
    this->m_but_pause->setEnabled(false);
    this->m_but_step->setEnabled(false);
    QMessageBox::information(this, "", "Fin de la simulation !");
}

/**
 * @brief Handle step-1 button's enabled state when the play/pause button is pressed
 */
void MainWindow::onPausePressed(){
    if (m_but_step->isEnabled())
        this->m_but_step->setEnabled(false);
    else
        this->m_but_step->setEnabled(true);
}

/**
 * @brief Handle the enabled state of the buttons when the reset button is pressed.
 */
void MainWindow::onResetPressed(){
    this->m_but_pause->setEnabled(true);
    this->m_but_pause->setChecked(false);
    this->m_but_step->setEnabled(true);
}

/**
 * @brief Update the parameters of the simulation.
 * @param new_width Grid width value
 * @param new_height Grid height value
 * @param new_pixel Pixel size of the grid value
 * @param new_sleep Sleeping time value
 * @param new_ant Number of ants value
 * @param new_orient Ant orientation policy value
 */
void MainWindow::updateParam(int new_width, int new_height, int new_pixel, int new_sleep,
                             int new_ant, QString new_orient){
    if (m_nb_col == new_width && m_nb_row == new_height && m_pixel == new_pixel
            && m_nb_ant == new_ant && m_orient == new_orient) {
        if (m_sleep != new_sleep){ // Only the sleep time has changed
            m_sleep = new_sleep;
            emit newSleepValue(m_sleep);
        }
        // Nothing has changed
        return;
    } else { // Otherwise, update all values and reset the simulation
        m_nb_col = new_width;
        m_nb_row = new_height;
        m_pixel = new_pixel;
        m_sleep = new_sleep;
        m_nb_ant = new_ant;
        m_orient = new_orient;

        // Reset the grid and pixmap
        this->ReAllocPixmap();

        // Reset the enabled state of the buttons
        this->onResetPressed();

        // Reset the simulation object
        this->resetSimulation();
    }
}

/**
 * @brief Update the number of iterations on the GUI.
 * @param nb_iter Number of iterations of the simulation
 */
void MainWindow::updateIteration(int const & nb_iter){
    QString text(QString("Iter : ") + QString::number(nb_iter));
    m_iterations->setText(text);
    qApp->processEvents();
}

/**
 * @brief Select the file in which the simulation should be saved.
 */
void MainWindow::saveToCSV(){
    QString filepath = QFileDialog::getSaveFileName(this,
           "Save the simulation", PATH_2_SIMUL_FILES, "CSV files (*.csv)");

    if (! filepath.isEmpty()){
        qDebug() << "Save simulation to" << filepath << "...";
        emit simulationToSave(filepath, m_pixel);
    }
}

/**
 * @brief Select the simulation file to load.
 */
void MainWindow::loadCSV(){
    QString filepath = QFileDialog::getOpenFileName(this,
           "Open a simulation file", PATH_2_SIMUL_FILES, "CSV files (*.csv)");

    if (! filepath.isEmpty()){
        qDebug() << "Open simulation" << filepath << "...";

        // Reset the enabled state of the buttons
        this->onResetPressed();

        // Reset the grid and pixmap
        emit simulationToLoad(filepath);
    }
}

/**
 * @brief Overload of QMainWindow::closeEvent to stop the simulation before closing the MainWindow.
 */
void MainWindow::closeEvent(QCloseEvent *event){
    emit stopSimulation();
    QMainWindow::closeEvent(event);
}

/**
 * @brief Destructor.
 */
MainWindow::~MainWindow(){
    m_simulThread->quit();
    m_simulThread->wait();
    delete m_simulation;
    delete m_label;
    delete m_iterations;
    delete m_painter;
    delete m_pix;
    delete m_but_step;
    delete m_but_pause;
    delete m_but_reset;
}
