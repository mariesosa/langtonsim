// Custom
#include <sources/paramwidget.h>
#include <sources/square.h>

// Graphical
#include <QFormLayout>
#include <QHBoxLayout>

/**
 * @brief Construct a ParamWidget
 * @param parent Its parent widget
 */
ParamWidget::ParamWidget(QWidget *parent): QWidget(parent){
    m_title = new QLabel(this);
    m_title->setText("Configuration");
    m_title->setStyleSheet("font-weight: bold");

    // Define form to modify the height, width, pixel's size of the grid, etc.
    QFormLayout *param_layout = new QFormLayout(this);
    m_spin_width = new QSpinBox(this);  // Grid width setting
    m_spin_height = new QSpinBox(this); // Grid height setting
    m_spin_pixel = new QSpinBox(this);  // Pixel size of the grid setting
    m_spin_sleep = new QSpinBox(this);  // Sleeping time (between each iteration) setting
    m_spin_ant = new QSpinBox(this);    // Number of ants setting

    // Set the range of values for each SpinBox
    m_spin_width->setMinimum(10);
    m_spin_width->setMaximum(1000);
    m_spin_height->setMinimum(10);
    m_spin_height->setMaximum(1000);
    m_spin_pixel->setMinimum(1);
    m_spin_pixel->setMaximum(12);
    m_spin_sleep->setMaximum(1000);
    m_spin_ant->setMinimum(1);

    // Create a comboBox to define the ant orientation policy setting
    m_combo_orientation = new QComboBox(this);
    for(auto orient: ALLOWED_ANT_ORIENT)
        m_combo_orientation->addItem(orient);

    // Set the initial value of each fields
    this->resetValues();

    // Define cancel and apply buttons
    QHBoxLayout *param_buttons = new QHBoxLayout();
    m_but_reset_param = new QPushButton("reset");
    m_but_set_param = new QPushButton("apply");

    // Add tooltips to buttons
    m_but_reset_param->setToolTip("Reset default values");
    m_but_set_param->setToolTip("Apply value changes");

    // Connect them to slots
    connect(m_but_set_param, &QPushButton::clicked, this, &ParamWidget::sendUpdateValues);
    connect(m_but_reset_param, &QPushButton::clicked, this, &ParamWidget::resetValues);

    // Add them to a dedicated layout
    param_buttons->addWidget(m_but_reset_param);
    param_buttons->addWidget(m_but_set_param);

    // Add all elements to a form layout
    param_layout->addRow(m_title);
    param_layout->addRow(tr("&Width:"), m_spin_width);
    param_layout->addRow(tr("&Height:"), m_spin_height);
    param_layout->addRow(tr("&Pixel size:"), m_spin_pixel);
    param_layout->addRow(tr("&Sleep (in ms):"), m_spin_sleep);
    param_layout->addRow(tr("&Nb ants:"), m_spin_ant);
    param_layout->addRow(tr("&Ants orientation:"), m_combo_orientation);
    param_layout->addRow(param_buttons);
}


// Slots
/**
 * @brief Send updated settings values using the valueChanged signal.
 * @see valueChanged
 */
void ParamWidget::sendUpdateValues(){
    int new_width = m_spin_width->value();
    int new_height = m_spin_height->value();
    int new_pixel = m_spin_pixel->value();
    int new_sleep = m_spin_sleep->value()*1000;
    int new_ant = m_spin_ant->value();
    QString new_orient = m_combo_orientation->currentText();
    emit valueChanged(new_width, new_height, new_pixel, new_sleep, new_ant, new_orient);
}

/**
 * @brief Set settings values to given ones.
 * @param new_width Grid width value
 * @param new_height Grid height value
 * @param new_pixel Pixel size of the grid value
 * @param new_sleep Sleeping time value
 * @param new_ant Number of ants value
 * @param new_orient Ant orientation policy value
 */
void ParamWidget::setValues(int new_width, int new_height, int new_pixel, int new_sleep, int new_ant, QString new_orient){
    m_spin_width->setValue(new_width);
    m_spin_height->setValue(new_height);
    m_spin_pixel->setValue(new_pixel);
    m_spin_sleep->setValue(new_sleep);
    m_spin_ant->setValue(new_ant);
    m_combo_orientation->setCurrentText(new_orient);
}

/**
 * @brief Reset settings values to default.
 */
void ParamWidget::resetValues(){
    m_spin_width->setValue(NB_COL);
    m_spin_height->setValue(NB_ROW);
    m_spin_pixel->setValue(PIXEL);
    m_spin_sleep->setValue(SLEEP_TIME);
    m_spin_ant->setValue(NB_ANT);
    m_combo_orientation->setCurrentText(ANT_ORIENT_POLICY);
}
