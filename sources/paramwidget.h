#ifndef PARAMWIDGET_H
#define PARAMWIDGET_H

#include "square.h"

#include <QWidget>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>
#include <QComboBox>
#include <QStringList>

// Definition of default variable values
constexpr static int NB_COL = 60; ///< Default number of column in the grid
constexpr static int NB_ROW = 60; ///< Default number of rows in the grid
constexpr static int PIXEL = 10;  ///< Default pixel size for the grid representation
constexpr static int NB_ANT = 2;  ///< Default number of ants on the grid

/// Default sleeping time between each iteration (i.e. step) in macro-seconds, must be in [0, 100000]
constexpr static unsigned int SLEEP_TIME = 0;

// Orientation policy of the ants.
static QString RANDOM_ORIENT = "Random"; ///< QString defining a random orientation

/// Default orientation policy of the ants. Possible values: {"Random", "Up", "Down", "Left", "Right"}.
static QString ANT_ORIENT_POLICY = RANDOM_ORIENT;

/// Allowed orientation policy of the ants.
static QStringList ALLOWED_ANT_ORIENT = (QStringList() << RANDOM_ORIENT << orientation_str);

/**
 * @brief A custom widget to manage the simulation parameters from the interface.
 * It allows to set:
 * - the grid width, height and pixel size,
 * - the number of ants on the grid and the ant orientation policy to use at the
 *   initialization,
 * - the sleeping time between each iteration (i.e. step) of the simulation in ms.
 */
class ParamWidget: public QWidget
{
    Q_OBJECT

private:
    QLabel *m_title; // Widget title

    // Elements for the selection of the grid's width and height, number of ants, etc.
    QSpinBox *m_spin_width;
    QSpinBox *m_spin_height;
    QSpinBox *m_spin_pixel;
    QSpinBox *m_spin_sleep;
    QSpinBox *m_spin_ant;
    QComboBox *m_combo_orientation;

    // Buttons used to validate new parameters or reset values to default
    QPushButton *m_but_reset_param;
    QPushButton *m_but_set_param;

public:
    // Constructor
    ParamWidget(QWidget *parent = 0);

public slots:
    void sendUpdateValues();
    void resetValues();
    void setValues(int new_width, int new_height, int new_pixel, int new_sleep, int new_ant, QString new_orient);

signals:
    /**
     * @brief Emited when button "apply" is clicked. It sends all new settings values.
     * @param new_width Grid width value
     * @param new_height Grid height value
     * @param new_pixel Pixel size of the grid value
     * @param new_sleep Sleeping time value
     * @param new_ant Number of ants value
     * @param new_orient Ant orientation policy value
     */
    void valueChanged(int new_width, int new_height, int new_pixel, int new_sleep, int new_ant, QString new_orient);
};

#endif // PARAMWIDGET_H
