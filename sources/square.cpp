#include "square.h"
#include <QString>

/**
 * @brief Create a Square at position (0,0) with white background.
 */
Square::Square():m_col(0), m_line(0), m_color(WHITE){}

/**
 * @brief Create a Square at position (`col`, `line`) with `color` background color.
 * @param col Column index of the square
 * @param line Line index of the square
 * @param color Background color of the square
 */
Square::Square(int const & col, int const & line, color_ant const & color)
    :m_col(col), m_line(line), m_color(color){}

/**
 * @brief Set the background color of the square to `color`.
 * @param color Color to use as background
 */
void Square::setColor(color_ant const & color){
    m_color = color;
}

/**
 * @brief Inverse the background color of the square.
 * Set square color to WHITE if the square is colorated, otherwise set it to `color`.
 * @param color Color to use for the inversion of background
 */
void Square::inverseColor(color_ant const & color){
    if (m_color == WHITE)
        m_color = color;
    else m_color = WHITE;
}

// Functions used to show orientation and ant_color enum as strings
/**
 * @brief Transform an orientation as a QString.
 * @param orient The orientation specified as an enum value
 * @return The QString associated to `orient`
 */
QString orientAsQString(orientation const & orient){
    return orientation_str.at(orient);
}

/**
 * @brief Transform a color_ant as a QString.
 * @param color The color specified as an enum value
 * @return The QString associated to `color`
 */
QString colorAsQString(color_ant const & color){
    return color_ant_str.at(color);
}

/**
 * @brief Transform a QString as an orientation.
 * @param orient The orientation specified as a QString
 * @return The orientation enum value associated to `orient`
 */
orientation QStringAsOrient(QString const & orient){
    int index = orientation_str.indexOf(orient);
    if (index >= 0)
        return (orientation) index;
    else
        return U;
}
