// Custom
#include "simulinstance.h"
#include "paramwidget.h"

// Standard
#include <iostream>
#include <unistd.h>
#include <ctime>
#include <QDebug>

// Used for read/write CSV files
#include <QList>
#include <QStringList>
#include <QDir>
#include <QVariant>
#include "qtcsv/stringdata.h"
#include "qtcsv/variantdata.h"
#include "qtcsv/reader.h"
#include "qtcsv/writer.h"

using namespace std;

static vector<color_ant> COLORS = {RED, BLUE, GREEN, ORANGE, PURPLE, YELLOW, BLACK};

/**
 * @brief Create a SimulInstance object.
 * @param nb_col Number of column in the grid
 * @param nb_row Number of rows in the grid
 * @param nb_ant Number of ants on the grid
 * @param sleep Sleep time between each iteration of the simulation (in macro seconds)
 * @param orient Orientation policy of the ants, possible values: {"Random", "Up", "Down", "Left", "Right"}
 */
SimulInstance::SimulInstance(int const & nb_col, int const & nb_row, int const & nb_ant,
                             int const & sleep, QString const & orient):
    m_nb_iter(0), m_sleep(sleep), m_nb_ant(nb_ant), m_is_dead(false), m_go_on(false),
    m_nb_col(nb_col), m_nb_row(nb_row), m_ant_orient_policy(orient){
    this->initGrid(orient);
}

/**
 * @brief Initialize the grid and the vector of ants according to `orient` value.
 * @param orient Orientation policy (on the grid) to use for the ants
 */
void SimulInstance::initGrid(QString const & orient){
    // Initialize the grid object
    m_grid = new Grid(m_nb_col, m_nb_row);

    // Clean the vector of ants and re-create ants
    m_ants.clear();
    if (orient == RANDOM_ORIENT)
        this->initAntsRandom();
    else
        this->initAntsOrient(QStringAsOrient(orient));

    // Save the initial state of the vector of ants
    m_ants_init = m_ants;
}

/**
 * @brief Create a vector (of size m_nb_ants) of ants created with random position and orientation.
 */
void SimulInstance::initAntsRandom(){
    for (int i=0; i<m_nb_ant; i++) {
        Ant ant = Ant(*m_grid, COLORS[i % COLORS.size()]);
        m_ants.push_back(ant);
        qDebug() << ant;
    }
}

/**
 * @brief Create a vector (of size m_nb_ants) of ants created with a random position and the given orientation.
 * @param orient the orientation to use to create each ant.
 */
void SimulInstance::initAntsOrient(const orientation & orient){
    for (int i=0; i<m_nb_ant; i++) {
        Ant ant = Ant(*m_grid, orient, COLORS[i % COLORS.size()]);
        m_ants.push_back(ant);
        qDebug() << ant;
    }
}

/**
 * @brief Return true if the Ant is dead (i.e. outside the grid), false otherwise.
 */
bool antIsDead(Ant ant){
    return ant.isDead();
}

/**
 * @brief Run one step of the simulation.
 */
void SimulInstance::oneStep(){
    // Vector specifying square to update
    vector<tuple<int,int>> position_2_update;

    if (m_is_dead)
        qDebug() << "All ants are already dead!";

    // New ant vector and grid
    Grid new_grid = *m_grid;

    for (auto & ant : m_ants) {
        // Get the previous position of the ant and add it to the vector of positions to update
        tuple<int,int> previous = ant.getPosition();
        position_2_update.push_back(previous);

        // Inverse the square color and its empty state (i.e. if a ant is on it or not)
        new_grid.inverseColor(get<0>(previous), get<1>(previous), ant.getColor());

        // Move the ant
        bool is_dead = ant.move(*m_grid);
        if (is_dead){
             qInfo().nospace() << "Iter " << m_nb_iter << " : Ant " << ant.getId() << " (color: "
                     << ant.getColorAsQString() << ") outside the grid !";
        }
    }

    // Remove died ants from the vector
    m_ants.erase(remove_if(m_ants.begin(), m_ants.end(), antIsDead), m_ants.end());

    // Update the grid
    delete m_grid;
    m_grid = new Grid(new_grid);

    // Update the number of iteration
    m_nb_iter += 1;

    // Check if there is still ants
    m_is_dead = m_ants.empty();

    emit simulValueUpdated(m_grid, m_ants, position_2_update, m_nb_iter);
}

/**
 * @brief Run the entire simulation (until all ants are dead).
 */
void SimulInstance::run(){
    m_go_on = true;
    while (!m_is_dead && m_go_on) {
        this->oneStep();
        if (m_is_dead){
            emit simulFinished();
            break;
        }

        // Wait m_sleep_time seconds
        usleep(m_sleep);
    }
}

/**
 * @brief Pause the simulation.
 */
void SimulInstance::pause(){
    if (m_go_on)
        m_go_on = false;
    else
        this->run();
}

/**
 * @brief Stop the simulation.
 */
void SimulInstance::stop(){
    m_go_on = false;
}

/**
 * @brief Pause and re-create a new simulation (with new ant positions, etc.)
 * @param nb_col Number of column in the grid
 * @param nb_row Number of rows in the grid
 * @param nb_ant Number of ants on the grid
 * @param sleep Sleep time between each iteration of the simulation (in macro seconds)
 * @param orient Orientation policy of the ants, possible values: {"Random", "Up", "Down", "Left", "Right"}
 */
void SimulInstance::createNewSimulation(int const & nb_col, int const & nb_row, int const & nb_ant,
                                        int const & sleep, QString const & orient){
    m_go_on = false;

    // Reset variables
    m_nb_iter = 0;
    m_nb_ant = nb_ant;
    m_is_dead = false;
    m_nb_col = nb_col;
    m_nb_row = nb_row;
    m_sleep = sleep;
    m_ant_orient_policy = orient;

    // Delete the old pointer on the grid
    delete m_grid;

    // Re-initialize the grid and ants positions
    this->initGrid(orient);

    // Save the initial state of the vector of ants
    m_ants_init = m_ants;

    emit simulValueReseted(m_grid, m_ants);
}

/**
 * @brief Pause and reset the simulation to its initial state.
 */
void SimulInstance::resetToInit(){
    m_go_on = false;

    // Reset variables
    m_nb_iter = 0;
    m_is_dead = false;

    // Delete the old pointer on the grid
    delete m_grid;
    m_grid = new Grid(m_nb_col, m_nb_row);

    // Clear the vector of ants
    m_ants.clear();
    m_ants = m_ants_init;

    emit simulValueReseted(m_grid, m_ants);
}

/**
 * @brief Update the sleep attribute of the simulation object.
 * @param sleep Sleep time between each iteration of the simulation (in macro seconds)
 */
void SimulInstance::updateSleep(int const & sleep){
    m_sleep = sleep;
}

/**
 * @brief Destructor
 */
SimulInstance::~SimulInstance(){
    delete m_grid;
}

/**
 * Save the initial state of the simulation to filepath.
 *
 * The generated file is of the following form:
 *
 *      <grid_width>,<grid_height>,<pixel_size_of_the_grid>,<ant_orientation_policy>
 *      <ID_ant_1>,<grid_column_position>,<grid_line_position>,<ant_orientation>,<ant_color>
 *      ...
 *      <ID_ant_N>,<grid_column_position>,<grid_line_position>,<ant_orientation>,<ant_color>
 *
 * Where items can have the following types and/or values:
 * - grid_width: unsigned int.
 * - grid_height: unsigned int.
 * - pixel_size_of_the_grid: int included in [1, 12].
 * - ant_orientation_policy: string of {"Random", "Up", "Down", "Left", "Right"}.
 * - ID: unsigned int.
 * - grid_column_position: int included in [0, grid_width[.
 * - grid_line_position: int included in [0, grid_height[.
 * - ant_orientation: int included in [0, 4].
 * - ant_color: int included in [0, 6].
 *
 * @note All items are written between quotation marks in the file.
 *
 * @param filepath Path to the CSV file in which the simulation will be saved.
 * @param pixel Current pixel size of the grid in the application.
 */
void SimulInstance::saveSimulationAsCSV(QString const & filepath, int const & pixel){
    QtCSV::VariantData strData;

    // Add data associated to the grid (width, height, pixel size)
    QList<QVariant> gridInfo;
    gridInfo << m_grid->getWidth() << m_grid->getHeight() << pixel << m_ant_orient_policy;
    strData.addRow(gridInfo);

    // Add data associated to each ant
    for(auto ant : m_ants_init){
        QList<QVariant> antInfo;
        antInfo << ant.getId() << ant.getCol() << ant.getLine() << ant.getOrient() << ant.getColor();
        strData.addRow(antInfo);
    }

    // Write all data to filepath
    QtCSV::Writer::write(filepath, strData);
    qInfo() << "Simulation saved.";
}

/**
 * Load the initial state of the simulation stored in `filepath`.
 *
 * If `filepath` is a correct simulation file, emit a simulLoaded signal.
 * Otherwise, emit an errorFound signal.
 *
 * @param filepath Path to the CSV file to load.
 * @see saveSimulationAsCSV, simulLoaded, errorFound
 */
void SimulInstance::loadSimulationFromCSV(QString const & filepath){
    // Stop the old simulation
    this->stop();

    // Create a CSV reader object
    QList<QStringList> readData = QtCSV::Reader::readToList(filepath);

    // Variables used to update simulation values
    vector<Ant> new_ants;
    int pixel_size, nb_col, nb_row;
    QString ant_orient;

    // Define error messages
    const char * missing_info_msg = "Not enough information";
    const char * wrong_type_msg = "Wrong information type";

    try{
        // Read the information about the grid (number of columns, row, pixel size)
        QStringList first_line = readData.at(0);

        // Throw an exception if the first line doesn't have enough elements
        if (first_line.size() < 4)
            throw CSVReadException(missing_info_msg, filepath.toUtf8(), 0);

        // Boolean specifying if the conversion from QString to int was succesful
        bool ok1, ok2, ok3 = true;

        // Read and convert information about the grid into ints
        nb_col = first_line.at(0).toUInt(&ok1);
        nb_row = first_line.at(1).toUInt(&ok2);
        pixel_size = first_line.at(2).toUInt(&ok3);

        // Read the information regarding the ant orientation policy
        ant_orient = first_line.at(3);

        // Throw an exception if at least one of the readed elements doesn't have the
        // correct type (int or orientation included in ALLOWED_ANT_ORIENT
        if (! (ok1 && ok2 && ok3 && ALLOWED_ANT_ORIENT.contains(ant_orient)))
            throw CSVReadException(wrong_type_msg, filepath.toUtf8(), 0);

        qDebug().nospace() << "Grid size = " << nb_col << " - " << nb_row << " (pixel size: " << pixel_size << ")";

        // Read the information about ants
        for (int i = 1; i < readData.size(); ++i){
            vector<int> ant_values;

            // Throw an exception if the line doesn't have enough elements
            if (readData.at(i).size() < 5)
                throw CSVReadException(missing_info_msg, filepath.toUtf8(), i);

            for (int j = 0; j < readData.at(i).size(); ++j){
                int value = readData.at(i).at(j).toUInt(&ok1);
                // If the readed information as the correct type (i.e. int), add it
                // to the vector of ant's values, otherwise throw an exception.
                if (ok1)
                    ant_values.push_back(value);
                else
                    throw CSVReadException(wrong_type_msg, filepath.toUtf8(), i);
            }

            // Throw an exception if the position of the ant is outside the grid
            if (0 > ant_values[1] || ant_values[1] >= nb_col || 0 > ant_values[2] || ant_values[2] >= nb_row)
                throw CSVReadException("Ant position outside the grid", filepath.toUtf8(), i);

            // Throw an exception if the orientation or the color is not a correct enum value
            if (ant_values[3] < U || ant_values[3] > R)
                throw CSVReadException("Ant's orientation isn't a correct enum value", filepath.toUtf8(), i);

            if (ant_values[4] < RED || ant_values[4] > BLACK)
                throw CSVReadException("Ant's color isn't a correct enum value", filepath.toUtf8(), i);

            // Create an ant from the readed values
            Ant ant = Ant(ant_values[1], ant_values[2], (orientation) ant_values[3], (color_ant) ant_values[4]);
            qDebug() << ant;
            new_ants.push_back(ant);
        }
    } catch (CSVReadException & error){
        qWarning().nospace() << error.what() << error.get_msg() << ", file: "
                             << error.get_file() << ", line: " << error.get_line();
        emit errorFound("Error while importing the simulation file.", error);
        return;
    } catch (const std::exception & error) {
        qWarning() << "Error while importing CSV file:" << filepath;
        qWarning() << error.what();
        return;
    }

    // Save simulation variables readed from file
    m_ant_orient_policy = ant_orient;
    m_nb_col = nb_col;
    m_nb_row = nb_row;

    // Re-initialize the grid
    delete m_grid;
    m_grid = new Grid(m_nb_col, m_nb_row);

    // Re-initialize the vector of ants
    m_ants.clear();
    m_ants = new_ants;

    // Save the initial state of the vector of ants
    m_ants_init = m_ants;

    // Reset other simulation variables
    m_nb_iter = 0;
    m_nb_ant = m_ants.size();
    m_is_dead = false;

    qInfo() << "Simulation created from CSV.";

    emit simulLoaded(m_grid, m_ants, pixel_size, m_ant_orient_policy);
}

/**
 * @brief Create a CSVReadException object.
 * @param msg Detailed exception message to show
 * @param file File for which the exception occurs
 * @param line Line number in which the exception occurs
 */
CSVReadException::CSVReadException(const char *msg, const char* file, int line): std::exception(),
    m_msg(msg), m_file(file), m_line(line){}
