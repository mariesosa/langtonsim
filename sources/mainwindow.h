#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Custom
#include "paramwidget.h"
#include "grid.h"
#include "ant.h"
#include "simulinstance.h"

// Graphical
#include <QMainWindow>
#include <QPixmap>
#include <QPainter>
#include <QLabel>
#include <QPushButton>
#include <QThread>
#include <QDir>

// Standard
#include <tuple>
#include <vector>

/// Default path where simulation files are stored
static QString PATH_2_SIMUL_FILES = QDir::currentPath() + "/simulations";

/// Default pixel size from which display change
static const int MIN_PIX_SHOW_GRID = 5;

/**
 * @brief MainWindow showing the grid and ant's moves (to the left) and buttons to handle
 * simulation actions and settings (to the right).
 *
 * It is composed of:
 * - a SimulInstance object,
 * - a QPixmap to show the graphical representation of the grid,
 * - a ParamWidget to handle simulation settings,
 * - a set of buttons to perform actions on the simulation (play/pause, reset, save, etc.),
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    // Computational
    SimulInstance *m_simulation; ///< Simulation object
    QThread *m_simulThread;      ///< Thread for the simulation object

    int m_nb_col;     ///< Number of column in the grid
    int m_nb_row;     ///< Number of rows in the grid
    int m_pixel;      ///< Pixel size for the grid representation
    int m_sleep;      ///< Sleeping time between each iteration (i.e. step) in macro-seconds
    int m_nb_ant;     ///< Number of ants on the grid
    QString m_orient; ///< Orientation policy of the ants

    // Graphical
    QLabel *m_label;           ///< Show `m_pix`
    QLabel *m_iterations;      ///< Show the number of iterations of the simulation
    QPainter *m_painter;       ///< Used to paint `m_pix`
    QPixmap *m_pix;            ///< QPixmap representing the grid
    ParamWidget *param_widget; ///< ParamWidget for simulation settings

    // Buttons associated to actions on the simulation
    QPushButton *m_but_step;       ///< Perform 1-step of the simulation
    QPushButton *m_but_pause;      ///< Pause the simulation
    QPushButton *m_but_reset;      ///< Reset the simulation to its initial state
    QPushButton *m_but_new;        ///< Create a new simulation
    QPushButton *m_but_save_simul; ///< Save the simulation to a CSV file
    QPushButton *m_but_load_simul; ///< Open a simulation from a CSV file

protected:
     void closeEvent(QCloseEvent *event);

public:
    // Constructor
    MainWindow(QWidget *parent = 0, int const & nb_col = NB_COL, int const & nb_row = NB_ROW,
               int const & pixel = PIXEL, int const & sleep = SLEEP_TIME);

    // Public methods
    void initPixmap(Grid *grid, std::vector<Ant> const & ants);
    void updateSquare(Grid *grid, int const & col, int const & line);
    void updatePixmap(Grid *grid, std::vector<Ant> const & ants, std::vector<std::tuple<int,int>> const & position_2_update);
    void ReAllocPixmap();
    void drawAnt(Ant const & ant);
    void updateIteration(int const & nb_iter);

    // Destructor
    ~MainWindow();

public slots:
    // Handle button's enabled/disabled state
    void onPausePressed();
    void onResetPressed();

    // Handle updates of simulation parameters and content
    void updateParam(int new_width, int new_height, int new_pixel, int new_sleep, int new_ant, QString new_orient);
    void initGui(Grid *grid, std::vector<Ant> const & ants);
    void updateGui(Grid *grid, std::vector<Ant> const & ants, std::vector<std::tuple<int,int>> position_2_update, int const & nb_iter);
    void loadCSV();
    void saveToCSV();
    void resetGui(Grid *grid, std::vector<Ant> const & ants);
    void resetSimulation();
    void endOfSimulation();
    void resetGuiAfterLoad(Grid *grid, std::vector<Ant> const & ants, int const & pixel_size, QString const & orient);
    void showErrorMessage(QString const & short_msg, CSVReadException const & error);

signals:
    /**
     * @brief Emited to re-create a new simulation from settings values.
     */
    void resetSimulValues(int const & nb_col, int const & nb_row, int const & nb_ant, int const & sleep, QString const & orient);

    /**
     * @brief Emited when (only) the sleeping time setting has changed,
     * to update the sleeping time of the simulation.
     */
    void newSleepValue(int const & sleep);

    /**
     * @brief Emited to open a simulation from file `filepath`.
     */
    void simulationToLoad(QString const & filepath);

    /**
     * @brief Emited to save a simulation to `filepath` (specifying the actual pixel size).
     */
    void simulationToSave(QString const & filepath, int const & pixel);

    /**
     * @brief Emited to update values of the ParamWidget when a simulation is opened from a CSV file.
     */
    void newParamFromLoad(int new_width, int new_height, int new_pixel, int new_sleep, int new_ant, QString const & new_orient);

    /**
     * @brief Emited to stop the simulation.
     */
    void stopSimulation();
};

QColor associateColor(color_ant const & color);

#endif // MAINWINDOW_H
