CONFIG *= qt
QT *= core

INCLUDEPATH += $$PWD

SOURCES += \
    $$PWD/sources/ant.cpp \
    $$PWD/sources/square.cpp \
    $$PWD/sources/grid.cpp \
    $$PWD/sources/mainwindow.cpp \
    $$PWD/sources/paramwidget.cpp \
    $$PWD/sources/simulinstance.cpp

HEADERS += \
    $$PWD/sources/ant.h \
    $$PWD/sources/square.h \
    $$PWD/sources/grid.h \
    $$PWD/sources/mainwindow.h \
    $$PWD/sources/paramwidget.h \
    $$PWD/sources/simulinstance.h

# Include qtcsv library
include("lib/qtcsv/qtcsv.pri")

# Include QtFontIcon library
include("lib/QFontIcon/QFontIcon/QFontIcon.pri")
