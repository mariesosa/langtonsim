# langtonSim 

A [Langton's ant](https://en.wikipedia.org/wiki/Langton%27s_ant) simulation
application implemented in C++ and Qt5.

It was tested on Kubuntu 18.04 with Qt >= 5.9.5.

## Dependencies

langtonSim depends on [qtcsv](https://github.com/iamantony/qtcsv) and
[FIcon](https://github.com/dridk/QFontIcon.git). Both are included in this
project as sub-modules.

## Installation

    git clone --recurse-submodules https://gitlab.com/mariesosa/langtonsim.git

## Build

Go to the cloned directory, then:

    # Create a build directory
    mkdir build
    cd build

    # Build the project
    qmake ../langtonSim.pro
    make
